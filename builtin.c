/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 18:52:37 by yaitalla          #+#    #+#             */
/*   Updated: 2016/01/02 16:54:29 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void				ft_env(t_shell *shell)
{
	int					i;

	i = 0;
	while (shell->env[i])
	{
		ft_putendl(shell->env[i]);
		i++;
	}
}

void				save(t_shell **shell)
{
	char				*pwd;

	pwd = NULL;
	pwd = getcwd(pwd, 0);
	free((*shell)->old_pwd);
	(*shell)->old_pwd = ft_strdup((*shell)->pwd);
	free((*shell)->pwd);
	(*shell)->pwd = pwd;
	ft_setenv("OLDPWD", (*shell)->old_pwd, shell);
	ft_setenv("PWD", pwd, shell);
}

void				ft_cd(t_shell **shell, char **arg)
{
	char				*path;

	if (ft_tablen(arg) == 1)
		path = ft_strdup((*shell)->home);
	else if (ft_strcmp(arg[1], "-") == 0)
		path = ft_strdup((*shell)->old_pwd);
	else
	{
		if (arg[1][0] == '/')
			path = ft_strdup(arg[1]);
		else
		{
			path = (char *)ft_memalloc(ft_strlen((*shell)->pwd) +
				ft_strlen(arg[1]) + 2);
			ft_strcat(path, (*shell)->pwd);
			ft_strcat(path, "/");
			ft_strcat(path, arg[1]);
		}
	}
	ft_direct(shell, path, arg[1]);
	free(path);
}

void				built(char **arg, t_shell *shell)
{
	int				i;

	i = HST->histindex > 0 ? HST->histindex - 1 : -1;
	if (ft_strcmp(arg[0], "exit") == 0)
		ft_exit(&shell, &arg);
	else if (ft_strcmp(arg[0], "cd") == 0)
		ft_cd(&shell, arg);
	else if (ft_strcmp(arg[0], "env") == 0)
		ft_env(shell);
	else if (ft_strcmp(arg[0], "setenv") == 0)
		ft_checkenv(arg, &shell, 1);
	else if (ft_strcmp(arg[0], "unsetenv") == 0)
		ft_checkenv(arg, &shell, 2);
	else if (ft_strcmp(arg[0], "prompt") == 0)
		shell->prompt = shell->prompt == 0 ? 1 : 0;
	else if (ft_strcmp(arg[0], "hist") == 0)
	{
		while (i >= 0)
		{
			ft_putendl(HST->hist[i]);
			i--;
		}
	}
	else
		putcolor(arg[0], GREEN, 1, 1);
}

void				ls_check(char **path)
{
	if (ft_strcmp(*path, "ls") == 0)
		*path = ft_strdup("/bin/ls");
}
