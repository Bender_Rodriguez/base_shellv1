/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 11:12:06 by yaitalla          #+#    #+#             */
/*   Updated: 2016/01/02 20:16:08 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static void		the_specialist(t_shell *shell, char buf[3])
{
	if (K_BAKSP && shell->cursor)
	{
		tputs(tgetstr("le", NULL), 1, tputchar);
		tputs(tgetstr("dc", NULL), 1, tputchar);
		shell->cursor--;
	}
}

static void		historic(t_shell *shell, char buf[3])
{
	int				i;

	i = shell->trial.cols - shell->cursor - 9;
	if (K_UP && HST->histindex > 0)
	{
		tputs(tgetstr("dl", NULL), 1, tputchar);
		while (i)
		{
			tputs(tgetstr("le", NULL), 1, tputchar);
			i--;
		}
		prompter(shell);
		ft_putstr(HST->hist[HST->histindex - 1]);
		shell->cursor = ft_strlen(HST->hist[HST->histindex - 1]);
		shell->cmd = ft_strdup(HST->hist[HST->histindex - 1]);
		HST->histindex--;
	}
	if (K_DOWN && HST->histindex < HST->len)
	{
		tputs(tgetstr("dl", NULL), 1, tputchar);
		while (i)
		{
			tputs(tgetstr("le", NULL), 1, tputchar);
			i--;
		}
		HST->histindex++;
		prompter(shell);
		ft_putstr(HST->hist[HST->histindex - 1]);
		shell->cmd = ft_strdup(HST->hist[HST->histindex - 1]);
		HST->histindex++;
	}
}

static void		the_arrow(t_shell *shell, char buf[3])
{
	if (K_LEFT && shell->cursor)
	{
		tputs(tgetstr("le", NULL), 1, tputchar);
		shell->cursor--;
	}
	else if (K_RIGHT)
	{
		tputs(tgetstr("nd", NULL), 1, tputchar);
		shell->cursor++;
	}
	else if (K_UP || K_DOWN)
		historic(shell, buf);
}

void			special_key(t_shell *shell, char buf[3])
{
	if (ARROW)
		the_arrow(shell, buf);
	if (SPECIAL)
		the_specialist(shell, buf);
}
