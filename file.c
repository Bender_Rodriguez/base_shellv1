/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/09 17:07:12 by yaitalla          #+#    #+#             */
/*   Updated: 2016/01/02 16:55:33 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void			prompter(t_shell *shell)
{
	if (shell->prompt == 1)
	{
		putcolor("21_SH", BROWN, 1, 0);
		putcolor(" $> ", MAGENTA, 1, 0);
	}
	else if (shell->prompt == 0)
	{
		putcolor(shell->pwd, BROWN, 1, 0);
		putcolor(" $> ", MAGENTA, 1, 0);
	}
	else if (shell->prompt == 2)
	{
		putcolor(ft_itoa(HST->histindex), BROWN, 1, 0);
		putcolor(" $> ", MAGENTA, 1, 0);
	}
}

int				lstlen(t_list *lst)
{
	int				i;
	t_list			*temp;

	i = 0;
	temp = lst;
	while (temp->next)
	{
		temp = temp->next;
		i++;
	}
	return (i);
}
