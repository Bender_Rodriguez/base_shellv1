/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 11:05:20 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/26 19:41:26 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int					main(void)
{
	t_shell				*shell;
	extern char			**environ;

	shell = init_all(environ);
	while (1)
		prompt(shell);
	termclose(shell);
	return (0);
}
